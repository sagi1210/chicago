### How to build Chicago ###

Before building:

1. `$ git clone https://sagi1210@bitbucket.org/sagi1210/chicago.git`
2. Copy all the files (`make-3.82/`, `CMakeLists.txt`, `*.in`) into your Chicago root directory.
3. Locate the sub-directories of `android`, `rogue`, and `rogue_km`, which look like `android-4.4.4_r2-orig`, `GPU/rogue-20151020`, and `GPU/rogue_km-20151020`.
4. Create symbolic links:

   * `$ ln -s android-4.4.4_r2-orig android`
   * `$ ln -s GPU/rogue-20151020 GPU/rogue`
   * `$ ln -s GPU/rogue_km-20151020 GPU/rogue_km`


Here are the steps to build (in the Chicago root directory):

* `$ mkdir build`
* `$ cd build`
* `$ cmake ..`
* `$ make`